/**
 * Date: 11.09.14
 * Time: 22:23
 * @file
 *
 */

(function ($, Drupal, window, document, undefined) {
  'use strict';
  // the app module
  Drupal.behaviors.angelApp = {
    attach: function () {
      var app = angular.module('angel', []);
    }
  };

})(jQuery, Drupal, this, this.document);

